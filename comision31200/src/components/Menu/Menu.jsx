import {Navbar, Container, Nav , NavDropdown} from 'react-bootstrap'

function Menu({param1}) {
    // const {param1} = props
    // console.log(props)
    return (
        <>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#home">Inicio</Nav.Link>
                        <Nav.Link href="#link">Gorras</Nav.Link>                    
                        <Nav.Link href="#link">Remeras</Nav.Link>                                                            
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            {/* <h2>{param1}</h2> */}
        </>
    )
}

export default Menu