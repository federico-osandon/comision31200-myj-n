import { useState, useEffect } from 'react'
import Cuerpo from './components/Cuerpo/Cuerpo'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/Menu/Menu';
import ComponenteClase from './components/ComponenteClase';
import Titulo from './components/Titulo/Titulo';
import ComponentContainer from './components/ComponentContainer/ComponentContainer';

//Menu({paramatro1:valor, parametro2,....})




function App() {    
    const [ count, setCount ] = useState(0)
    const [bool, setBool] = useState(true)
    // const [ count, modificarCount ] = useState(0)
    // let count = 0;
    // console.log(estado[1])

    function agregar() {       
        // console.log()
        setCount(count + 1)
    }

    const cambiarEstado = () => {
        setBool(!bool)
    }

    useEffect(()=>{
        console.log('addEventListener')
        return ()=>{
            console.log('removeEventListener')
        }
    })

    useEffect(()=>{
        console.log('llamada a una api 2 una sola vez, despues de montado')
    }, [])
    
    // useEffect(()=>{
    //     console.log('solo cuando se modifique bool 3')
    // }, [bool])

    // console.log('renderizado de app 4')



    return (
        <div>
            <h2>la cantidad es = {count}</h2>
            <button onClick={agregar}>click</button>      
            <button onClick={cambiarEstado}>bool</button>      
        </div>      
    )
}

//App()

export default App
